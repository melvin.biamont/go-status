package failure

type FailureResponse struct {
	Code        int    `json:"code"`
	Failure     string `json:"error"`
	Description string `json:"description,omitempty"`
}

func NewFailure(code int, failure string) FailureResponse {
	return FailureResponse{Code: code, Failure: failure}
}

func NewFailureWithDescription(code int, failure string, description string) FailureResponse {
	return FailureResponse{Code: code, Failure: failure, Description: description}
}
