package success

type Status struct {
	Status string `json:"status"`
}

func NewStatus(s string) *Status {
	return &Status{Status: s}
}
